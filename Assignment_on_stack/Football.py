from stack import *
class Football(Stack):
	def main():
	    cases = input()
	    for begin in range(cases):
		arg = map(int, raw_input().split())
		stack = Stack()
		last = 0
		stack.push(arg[1])
		for playerid in range(arg[0]):
		    cur = raw_input().split()
		    if (cur[0] == 'P'):
		        stack.push(int(cur[1]))
		        last = 0
		    else:
		        temp = stack.pop()
			if(last != 0):
				stack.push(last)
			last = temp

		print "Player", stack.pop()
	main()
