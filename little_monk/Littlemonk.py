class Stack:
        
    def __init__(self):
        self.top = -1
        self.st = []
        
    def push(self,n):
        self.st.append(n)
        self.top+=1
        
    def pop(self):
        if self.top==-1:
            return
        self.top-=1
        return self.st.pop()
        
    def peek(self):
        return self.st[self.top]
        
    def isEmpty(self):
        if self.top==-1:
            return True
        return False
        
    def isMaximum(self, first, second):
        if first > second:
            return first
        else:
            return second
     
def main():
    number_of_input = raw_input()
    array = map(int,raw_input().strip().split())
    stack = Stack()
    ans = 0
    count = 0
    for value in array:
        if value > 0:
            stack.push(value)
        else:
            if stack.isEmpty():
                ans = stack.isMaximum(ans,count)
                count = 0
            else:
                if stack.peek() == -value:
                    stack.pop()
                    count+=2
                        
                    if stack.isEmpty():
                        ans = stack.isMaximum(ans,count) 
     
    print ans
     
main()
