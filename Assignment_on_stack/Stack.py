class Stack:
        
    def __init__(self):
        self.top = -1
        self.st = []
        
    def push(self,n):
        self.st.append(n)
        self.top+=1
        
    def pop(self):
        if self.top==-1:
            return
        self.top-=1
        return self.st.pop()
        
    def peek(self):
        return self.st[self.top]

    def empty(self):
        while self.top!=-1:
            self.top-=1 
        return  	
        
    def isEmpty(self):
        if self.top==-1:
            return True
        return False

    def isMaximum(self, first, second):
        if first > second:
            return first
        else:
            return second	

